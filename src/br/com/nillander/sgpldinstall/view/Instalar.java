package br.com.nillander.sgpldinstall.view;

import br.com.nillander.sgpldinstall.control.InstaladorController;
import br.com.nillander.sgpldinstall.model.Constantes;
import java.awt.Color;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;

public class Instalar extends javax.swing.JFrame {

    private static Instalar instance = null;

    public static Instalar getInstance() {
        instance = (instance == null) ? new Instalar() : instance;
        return instance;
    }

    public Instalar() {
        initComponents();
        inicializadores();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        lbl1Step = new javax.swing.JLabel();
        lbl2Step = new javax.swing.JLabel();
        lbl3Step = new javax.swing.JLabel();
        lbl4Step = new javax.swing.JLabel();
        lblClose = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        lblBg = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setAutoRequestFocus(false);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbl1Step.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/nillander/sgpldinstall/view/1step.png"))); // NOI18N
        lbl1Step.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl1StepMouseClicked(evt);
            }
        });
        jPanel1.add(lbl1Step, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 280, -1, -1));

        lbl2Step.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/nillander/sgpldinstall/view/2step.png"))); // NOI18N
        lbl2Step.setEnabled(false);
        lbl2Step.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl2StepMouseClicked(evt);
            }
        });
        jPanel1.add(lbl2Step, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 280, -1, -1));

        lbl3Step.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/nillander/sgpldinstall/view/3step.png"))); // NOI18N
        lbl3Step.setEnabled(false);
        lbl3Step.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl3StepMouseClicked(evt);
            }
        });
        jPanel1.add(lbl3Step, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 340, -1, -1));

        lbl4Step.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/nillander/sgpldinstall/view/btnOpen.png"))); // NOI18N
        lbl4Step.setEnabled(false);
        lbl4Step.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl4StepMouseClicked(evt);
            }
        });
        jPanel1.add(lbl4Step, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 340, -1, -1));

        lblClose.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/nillander/sgpldinstall/view/btnClose.png"))); // NOI18N
        lblClose.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblCloseMouseClicked(evt);
            }
        });
        jPanel1.add(lblClose, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 10, -1, -1));

        jTextArea1.setEditable(false);
        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane1.setViewportView(jTextArea1);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 300, 350, 140));

        lblBg.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/nillander/sgpldinstall/view/bg.png"))); // NOI18N
        jPanel1.add(lblBg, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 800, 450));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void lblCloseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblCloseMouseClicked
        fechar();
    }//GEN-LAST:event_lblCloseMouseClicked

    private void lbl1StepMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl1StepMouseClicked
        step1();
    }//GEN-LAST:event_lbl1StepMouseClicked

    private void lbl2StepMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl2StepMouseClicked
        step2();
    }//GEN-LAST:event_lbl2StepMouseClicked

    private void lbl3StepMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl3StepMouseClicked
        step3();
    }//GEN-LAST:event_lbl3StepMouseClicked

    private void lbl4StepMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl4StepMouseClicked
        step4();
    }//GEN-LAST:event_lbl4StepMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JLabel lbl1Step;
    private javax.swing.JLabel lbl2Step;
    private javax.swing.JLabel lbl3Step;
    private javax.swing.JLabel lbl4Step;
    private javax.swing.JLabel lblBg;
    private javax.swing.JLabel lblClose;
    // End of variables declaration//GEN-END:variables

    private void inicializadores() {
        setLocationRelativeTo(null);
        this.setBackground(new Color(0, 0, 0, 0));
        jPanel1.setBackground(new Color(0, 0, 0, 0));
        alterarAcaoAoFechar();
    }

    private void alterarAcaoAoFechar() {
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we) {
                fechar();
            }
        });
    }

    private void fechar() {
        if (InstaladorController.getInstance().confirmarDialog("Deseja Fechar?")) {
            InstaladorController.getInstance().excluirSeExistir(Constantes.SISTEMA_ZIP_ARQUIVO);
            InstaladorController.getInstance().excluirSeExistir(Constantes.ARQUIVO_PROCEDURE);
            System.exit(0);
        }
    }

    public void appendText(String texto) {
        new Thread(() -> {
            jTextArea1.append(texto + "\n");
            jTextArea1.setCaretPosition(jTextArea1.getDocument().getLength());
            repaint();
        }).start();
    }

    private void step1() {
        if (lbl1Step.isEnabled()) {
            lbl1Step.setEnabled(false);
            InstaladorController.getInstance().passo1();
            appendText("Instalando servidor de banco de dados (Vertrigo).");
            if (InstaladorController.getInstance().confirmarDialog("Você instalou corretamente o servidor Vertrigo?")) {
                lbl2Step.setEnabled(true);
            } else {
                lbl1Step.setEnabled(true);
            }
        }
    }

    private void step2() {
        new Thread(() -> {
            if (lbl2Step.isEnabled()) {
                new Thread(() -> {
                    lbl2Step.setEnabled(false);
                    appendText("Iniciando servidor de banco de dados (Vertrigo).");
                }).start();
                InstaladorController.getInstance().abrirArquivo(Constantes.WAMP_EXE);
                if (InstaladorController.getInstance().confirmarDialog("O Servidor Vertrigo está operando online?")) {
                    InstaladorController.getInstance().passo2();
                    lbl3Step.setEnabled(true);
                } else {
                    lbl1Step.setEnabled(true);
                }
            }
        }).start();
    }

    private void step3() {
        new Thread(() -> {
            if (lbl3Step.isEnabled()) {
                new Thread(() -> {
                    lbl3Step.setEnabled(false);
                }).start();
                if (InstaladorController.getInstance().passo3() == true) {
                    lbl4Step.setEnabled(true);
                } else {
                    lbl3Step.setEnabled(true);
                }
            }
        }).start();
    }

    private void step4() {
        if (lbl4Step.isEnabled()) {
            InstaladorController.getInstance().abrirJar();
        }
    }

}
