package br.com.nillander.sgpldinstall.model;

import br.com.nillander.sgpldinstall.control.InstaladorController;
import br.com.nillander.sgpldinstall.view.Instalar;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class InstaladorDAO {

    private static InstaladorDAO instance = null;

    public static InstaladorDAO getInstance() {
        instance = (instance == null) ? new InstaladorDAO() : instance;
        return instance;
    }

    public void configurarUsuariosMySQL() {
        MySQLDAO.getConnection_Root();
        MySQLDAO.executeQuery("ALTER USER '" + Constantes.WAMP_MYSQL_USER + "'@'localhost' IDENTIFIED BY '" + Constantes.WAMP_MYSQL_NEW_PASSWORD + "';");
        MySQLDAO.executeQuery("flush privileges;");

        MySQLDAO.executeQuery("GRANT ALL PRIVILEGES ON *.* TO '" + Constantes.SYSTEM_MYSQL_USER + "'@'%' IDENTIFIED BY '" + Constantes.SYSTEM_MYSQL_PASSWORD + "';");
        MySQLDAO.executeQuery("flush privileges;");
    }

    public void criarBaseDeDadosSGPLD() {
        MySQLDAO.getConnection_Root_Dev();
        MySQLDAO.executeQuery("DROP DATABASE IF EXISTS `" + Constantes.DATABASE_SYSTEM + "`;");
        MySQLDAO.executeQuery("DROP DATABASE IF EXISTS `" + Constantes.DATABASE_USER + "`;");

        MySQLDAO.executeQuery("CREATE DATABASE IF NOT EXISTS `" + Constantes.DATABASE_SYSTEM + "`;");
        MySQLDAO.executeQuery("CREATE DATABASE IF NOT EXISTS `" + Constantes.DATABASE_USER + "`;");
    }

    public void criarBaseDeDados(final String QUERY) {
        if (QUERY.contains("DELIMITER")) {
            abstrairEImplementarBaseComDelimitador(QUERY);
        } else {
            abstrairEImplementarBaseSomenteSql(QUERY);
        }
        Instalar.getInstance().appendText("Estrutura Importada com Sucesso!");
    }

    private void abstrairEImplementarBaseComDelimitador(String query) {
        String queries[] = query.split("DELIMITER");
        for (String querie : queries) {
            if (!querie.trim().isEmpty()) {
                if (!querie.equals(queries[queries.length - 1])) {
                    InstaladorController.getInstance().criarSeNaoExistir(Constantes.PASTA_TEMPORARIA);
                    InstaladorController.getInstance().excluirSeExistir(Constantes.ARQUIVO_PROCEDURE);

                    int processoCompleto;
                    String execucaoRunTime = "\"" + Constantes.MYSQL_EXE + "\" "
                            + "-hlocalhost -u" + Constantes.SYSTEM_MYSQL_USER
                            + " -p" + Constantes.SYSTEM_MYSQL_PASSWORD
                            + " -D" + Constantes.DATABASE_USER + " < "
                            + Constantes.ARQUIVO_PROCEDURE.getAbsolutePath();
                    try (FileWriter escreverNoArquivo = new FileWriter(Constantes.ARQUIVO_PROCEDURE, true)) {
                        escreverNoArquivo.write("DELIMITER " + querie + " DELIMITER ;");
                        escreverNoArquivo.close();

                        Process processo = Runtime.getRuntime().exec(
                                new String[]{"cmd.exe", "/c",
                                    execucaoRunTime
                                },
                                new String[]{});
                        processoCompleto = processo.waitFor();
                    } catch (IOException | InterruptedException ex) {
                        processoCompleto = 1337;
                        Logger.getLogger(InstaladorController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    if (processoCompleto != 0) {
                        Instalar.getInstance().appendText("ERRO: Procedures Não foram Importadas: " + processoCompleto);
                        Instalar.getInstance().appendText("=================================");
                        Instalar.getInstance().appendText("ATENÇÃO: CONTATE O ADMINISTRADOR");
                        Instalar.getInstance().appendText("=================================");
                    } else {
                        Instalar.getInstance().appendText("Procedures Importadas.");
                    }
                    InstaladorController.getInstance().excluirSeExistir(Constantes.ARQUIVO_PROCEDURE);
                } else {
                    abstrairEImplementarBaseSomenteSql(querie);
                }
            }
        }
    }

    private void abstrairEImplementarBaseSomenteSql(String QUERY_SQL) {
        String queries[] = QUERY_SQL.split(";");
        for (String querie : queries) {
            if (!querie.trim().isEmpty()) {
                MySQLDAO.executeQuery("SET FOREIGN_KEY_CHECKS = 0;");
                MySQLDAO.executeQuery(querie);
                MySQLDAO.executeQuery("SET FOREIGN_KEY_CHECKS = 1;");
            }
        }
    }

    public String lerCanalDeBytes(final String URI_ARQUIVO) {
        try {
            ByteArrayOutputStream baOutputStream = new ByteArrayOutputStream();
            WritableByteChannel wbcDestino = Channels.newChannel(baOutputStream);

            ReadableByteChannel rbcSource;
            rbcSource = Channels.newChannel(new URL(URI_ARQUIVO).openStream());
            ByteBuffer byteBuffer = ByteBuffer.allocate(1024);

            while (rbcSource.read(byteBuffer) != -1) {
                byteBuffer.flip();
                wbcDestino.write(byteBuffer);
                byteBuffer.clear();
            }
            rbcSource.close();
            return new String(baOutputStream.toByteArray(), "UTF-8");
        } catch (IOException ex) {
            Logger.getLogger(InstaladorController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void baixarAplicacaoZipada(final String URI_ARQUIVO, File DESTINO) {
        try {
            InstaladorController.getInstance().excluirSeExistir(Constantes.PASTA_INSTALACAO);
            InstaladorController.getInstance().criarSeNaoExistir(Constantes.PASTA_INSTALACAO);

            ReadableByteChannel rbcSource;
            rbcSource = Channels.newChannel(new URL(URI_ARQUIVO).openStream());
            FileOutputStream saidaArquivo;
            saidaArquivo = new FileOutputStream(DESTINO);
            saidaArquivo.getChannel().transferFrom(rbcSource, 0, Long.MAX_VALUE);
            saidaArquivo.close();
            rbcSource.close();

        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
            Logger.getLogger(InstaladorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
