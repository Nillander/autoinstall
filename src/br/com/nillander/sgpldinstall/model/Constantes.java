package br.com.nillander.sgpldinstall.model;

import java.io.File;

public class Constantes {

    // PASTAS
    public static final File PASTA_SETUP = new File("sgpld_setup");
    public static final File PASTA_INSTALACAO = new File("C:\\Sgpld");
    // PASTAS

    // Arquivos de instalação e aplicações do WAMP
    public static final File WAMP_INSTALL = new File(
            PASTA_SETUP.getAbsoluteFile().toString()
                    .concat(File.separator)
                    .concat("Vertrigo.exe")
    );
    public static final File WAMP_EXE = new File("C:\\Program Files\\VertrigoServ\\Vertrigo.exe");
    public static final File MYSQL_EXE = new File("C:\\Program Files\\VertrigoServ\\Mysql\\bin\\mysql.exe");
    // Arquivos de instalação e aplicações do WAMP

    // Arquivos Temporários
    public static final File PASTA_TEMPORARIA = new File(
            System.getProperty("java.io.tmpdir")
                    .concat(File.separator)
                    .concat("sgpld")
                    .concat(File.separator)
                    .concat("install")
    );
    public static final File ARQUIVO_PROCEDURE = new File(
            PASTA_TEMPORARIA.getPath()
                    .concat(File.separator)
                    .concat("sgpld_procedure.sql")
    );
    // Arquivos Temporários

    // Arquivos da Aplicação Principal
    public static final String SISTEMA_ZIP_NOME = "sgpld2019.zip";
    public static final File SISTEMA_ZIP_ARQUIVO = new File(
            PASTA_INSTALACAO.getAbsolutePath()
                    .concat(File.separator)
                    .concat(SISTEMA_ZIP_NOME)
    );

    public static final String SISTEMA_JAR_NOME = "SGPLD.jar";
    public static final File SISTEMA_JAR_ARQUIVO = new File(
            PASTA_INSTALACAO.getAbsolutePath()
                    .concat(File.separator)
                    .concat(SISTEMA_JAR_NOME)
    );

    public static final String SISTEMA_BATCH_NOME = "SGPLD.bat";
    public static final File SISTEMA_BATCH = new File(
            PASTA_INSTALACAO.getAbsolutePath()
                    .concat(File.separator)
                    .concat(SISTEMA_BATCH_NOME)
    );
    // Arquivos da Aplicação Principal

    // Variáveis de Banco de Dados
    public static final String WAMP_MYSQL_USER = "root";
    public static final String WAMP_MYSQL_PASSWORD = "wamp_senha_padrao";
    public static final String WAMP_MYSQL_NEW_PASSWORD = "wamp_senha_nova";
    public static final String SYSTEM_MYSQL_USER = "wamp_usuario_novo";
    public static final String SYSTEM_MYSQL_PASSWORD = "wamp_usuario_novo_senha";
    public static final String DATABASE_SYSTEM = "banco_de_dados_1";
    public static final String DATABASE_USER = "banco_de_dados_2";
    // Variáveis de Banco de Dados
}
