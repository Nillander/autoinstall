package br.com.nillander.sgpldinstall.model;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class MySQLDAO {

    public static final String DRIVER = "com.mysql.jdbc.Driver";
    public static String DBURL = "";
    private static Connection connection;

    private static void setDBURL(String DATABASE) {
        String SERVER = "localhost";
        DATABASE = (DATABASE == null) ? "" : DATABASE;
        String IP = null;
        try {
            InetAddress address = InetAddress.getByName(SERVER);
            IP = address.getHostAddress();
        } catch (UnknownHostException ex) {
            JOptionPane.showMessageDialog(null, "Não foi possível encontrar o servidor.\n" + ex.getMessage(), "Atenção!", JOptionPane.ERROR_MESSAGE);
        }
        DBURL = "jdbc:mysql://" + IP + ":3306/" + DATABASE;
    }

    public static Connection setConnection(String USER, String PASSWORD) {
        try {
            Class.forName(DRIVER).newInstance();
            connection = DriverManager.getConnection(DBURL, USER, PASSWORD);
            return connection;
        } catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            JOptionPane.showMessageDialog(null, "Falha ao Conectar com o Banco de Dados.\n" + ex.getMessage(), "Falha!", JOptionPane.WARNING_MESSAGE);
            Logger.getLogger(MySQLDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    // método para criar a conexão com o MySQL
    public static Connection getConnection_Root() {
        setDBURL(null);
        if (setConnection(Constantes.WAMP_MYSQL_USER, Constantes.WAMP_MYSQL_PASSWORD) == null) {
            JOptionPane.showMessageDialog(null, "Tentaremos novamente com outras credenciais.\n", "Atenção!", JOptionPane.WARNING_MESSAGE);
            getConnection_Root_Dev();
        }
        return connection;
    }

    public static Connection getConnection_Root_Dev() {
        setDBURL(null);
        if (setConnection(Constantes.WAMP_MYSQL_USER, Constantes.WAMP_MYSQL_NEW_PASSWORD) == null){
            JOptionPane.showMessageDialog(null, "Recomendamos a instalação desde o passo 1.\n"
                    + "Caso os erros persistam contate o ADMINISTRADOR", "Atenção!", JOptionPane.WARNING_MESSAGE);
        }
        return connection;
    }

    public static Connection getConnection_Setup() {
        setDBURL("sgpld_setup");
        setConnection(Constantes.SYSTEM_MYSQL_USER, Constantes.SYSTEM_MYSQL_PASSWORD);
        return connection;
    }

    public static Connection getConnection_Local() {
        setDBURL("sgpld_local");
        setConnection(Constantes.SYSTEM_MYSQL_USER, Constantes.SYSTEM_MYSQL_PASSWORD);
        return connection;
    }

    public static ResultSet getResultSet(String query, Object... parametros) {
        PreparedStatement psmt;
        ResultSet rs = null;
        try {
            psmt = connection.prepareStatement(query);
            for (int i = 0; i < parametros.length; i++) {
                psmt.setObject(i + 1, parametros[i]);
            }
            rs = psmt.executeQuery();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Falha ao obter resultados.\n" + ex.getMessage(), "Falha!", JOptionPane.ERROR_MESSAGE);
        }
        return rs;
    }

    public static int executeQuery(String query, Object... parametros) {
        long update = 0;
        PreparedStatement psmt;
        try {
            psmt = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);

            for (int i = 0; i < parametros.length; i++) {
                psmt.setObject(i + 1, parametros[i]);
            }

            psmt.execute();
            ResultSet rs = psmt.getGeneratedKeys();
            if (rs != null && rs.next()) {
                update = rs.getLong(1);
            }
            psmt.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Falha ao Executar Query.\n\n"
                    + ex.getMessage() + "\n\n"
                    + query, "Falha!", JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(MySQLDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (int) update;
    }

    public static void terminar() {
        try {
            (MySQLDAO.getConnection_Local()).close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Falha ao Fechar Conexão.\n" + ex.getMessage(), "Falha!", JOptionPane.ERROR_MESSAGE);
        }
    }

}
