package br.com.nillander.sgpldinstall.model;

public class WebServiceArquivos {

    private static WebServiceArquivos instance = null;

    public static WebServiceArquivos getInstance() {
        instance = (instance == null) ? new WebServiceArquivos() : instance;
        return instance;
    }

    public static String ultimaVersaoArquivoURI(String arquivo) {
        switch (arquivo) {
            case Constantes.DATABASE_SYSTEM:
                return "https://banco_de_dados_1.sql";

            case Constantes.DATABASE_USER:
                return "https://banco_de_dados_2.sql";

            case Constantes.SISTEMA_ZIP_NOME:
                return "https://aplicacao_jar.zip";
            default:
                return null;
        }
    }
}
