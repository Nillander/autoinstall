package br.com.nillander.sgpldinstall.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javax.swing.JOptionPane;

public class UnzipFile {

    private static UnzipFile instance = null;

    public static UnzipFile getInstance() {
        instance = (instance == null) ? new UnzipFile() : instance;
        return instance;
    }

    public void extrair(final File ARQUIVO_ZIP, final File PASTA_DESTINO) {
        try {
            byte[] buffer = new byte[1024];
            ZipInputStream zipInputStream;
            zipInputStream = new ZipInputStream(new FileInputStream(ARQUIVO_ZIP));
            ZipEntry zipEntry = zipInputStream.getNextEntry();
            while (zipEntry != null) {
                if (zipEntry.isDirectory()) {
                    File newFile = newFile(PASTA_DESTINO, zipEntry);
                    newFile.mkdirs();
                    zipEntry = zipInputStream.getNextEntry();

                } else {
                    File newFile = newFile(PASTA_DESTINO, zipEntry);
                    FileOutputStream arquivoOutputStream;
                    arquivoOutputStream = new FileOutputStream(newFile);
                    int len;
                    while ((len = zipInputStream.read(buffer)) > 0) {
                        arquivoOutputStream.write(buffer, 0, len);
                    }
                    arquivoOutputStream.close();
                    zipEntry = zipInputStream.getNextEntry();
                }
            }
            zipInputStream.closeEntry();
            zipInputStream.close();

        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
            Logger.getLogger(UnzipFile.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public File newFile(File destinationDir, ZipEntry zipEntry) throws IOException {
        File destFile = new File(destinationDir, zipEntry.getName());

        String destDirPath = destinationDir.getCanonicalPath();
        String destFilePath = destFile.getCanonicalPath();

        if (!destFilePath.startsWith(destDirPath + File.separator)) {
            JOptionPane.showMessageDialog(null, "A entrada está fora do dir de destino" + zipEntry.getName());
            throw new IOException("A entrada está fora do dir de destino: " + zipEntry.getName());
        }

        return destFile;
    }
}
