package br.com.nillander.sgpldinstall.control;

import br.com.nillander.sgpldinstall.model.Constantes;
import br.com.nillander.sgpldinstall.model.InstaladorDAO;
import br.com.nillander.sgpldinstall.model.MySQLDAO;
import br.com.nillander.sgpldinstall.model.UnzipFile;
import br.com.nillander.sgpldinstall.model.WebServiceArquivos;
import br.com.nillander.sgpldinstall.view.Instalar;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class InstaladorController {

    private static InstaladorController instance = null;

    public static InstaladorController getInstance() {
        instance = (instance == null) ? new InstaladorController() : instance;
        return instance;
    }

    // PASSOS
    public void passo1() {
        this.abrirArquivo(Constantes.WAMP_INSTALL);
    }

    public void passo2() {
        Instalar.getInstance().appendText("Criando rotas de acessos.");
        InstaladorDAO.getInstance().configurarUsuariosMySQL();

        Instalar.getInstance().appendText("Criando bases de dados.");
        InstaladorDAO.getInstance().criarBaseDeDadosSGPLD();

        MySQLDAO.getConnection_Setup();
        passo2importarBaseDeDadosSQL("Importando estrutura 1/2.", Constantes.DATABASE_SYSTEM);

        MySQLDAO.getConnection_Local();
        passo2importarBaseDeDadosSQL("Importando estrutura 2/2.", Constantes.DATABASE_USER);
    }

    private void passo2importarBaseDeDadosSQL(final String TEXTO_LOG, final String ARQUIVO_BASE_DE_DADOS_SQL) {
        Instalar.getInstance().appendText(TEXTO_LOG);
        final String URI_ARQUIVO_SQL_ATUALIZADO = WebServiceArquivos.ultimaVersaoArquivoURI(ARQUIVO_BASE_DE_DADOS_SQL);
        final String QUERY_CONTEUDO_SQL = InstaladorDAO.getInstance().lerCanalDeBytes(URI_ARQUIVO_SQL_ATUALIZADO);

        if (QUERY_CONTEUDO_SQL != null) {
            InstaladorDAO.getInstance().criarBaseDeDados(QUERY_CONTEUDO_SQL);
        } else {
            Instalar.getInstance().appendText("Erro!: " + URI_ARQUIVO_SQL_ATUALIZADO + " é nulo.");
        }
    }

    public boolean passo3() {
        Instalar.getInstance().appendText("Iniciando download da aplicação.");
        InstaladorDAO.getInstance().baixarAplicacaoZipada(
                WebServiceArquivos.ultimaVersaoArquivoURI(Constantes.SISTEMA_ZIP_NOME),
                Constantes.SISTEMA_ZIP_ARQUIVO
        );

        if (Constantes.SISTEMA_ZIP_ARQUIVO.exists()) {
            Instalar.getInstance().appendText("Extraindo arquivos.");
            UnzipFile.getInstance().extrair(Constantes.SISTEMA_ZIP_ARQUIVO, Constantes.PASTA_INSTALACAO);
            excluirSeExistir(Constantes.SISTEMA_ZIP_ARQUIVO);
            return true;
        } else {
            JOptionPane.showMessageDialog(null, "Arquivo de extração não encontrado");
            return false;
        }
    }

    // PASSOS FIM
    public boolean confirmarDialog(String mensagem) {
        return (JOptionPane.showConfirmDialog(null, mensagem) == JOptionPane.OK_OPTION);
    }

    public void abrirJar() {
        try {
            new ProcessBuilder("cmd.exe", "/c", "start " + Constantes.SISTEMA_BATCH.getAbsolutePath(), "exit").start();
            Instalar.getInstance().appendText("Iniciando aplicação...");
        } catch (IOException ex) {
            Logger.getLogger(InstaladorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void abrirArquivo(final File arquivo) {
        if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.OPEN)) {
            if (arquivo.exists()) {
                try {
                    Desktop.getDesktop().open(arquivo);
                } catch (IOException ex) {
                    JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro!", JOptionPane.ERROR_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(null, "O Arquivo não existe ou está corrompido.\nRecomendamos baixar a aplicação no passo 3.", "Atenção!", JOptionPane.WARNING_MESSAGE);
            }
        }
    }

    public void excluirSeExistir(File ARQUIVO_CONTEXTO) {
        if (ARQUIVO_CONTEXTO.exists()) {
            if (!ARQUIVO_CONTEXTO.isDirectory()) {
                ARQUIVO_CONTEXTO.delete();
            } else {
                String[] ARQUIVOS_DA_PASTA = ARQUIVO_CONTEXTO.list();
                for (String s : ARQUIVOS_DA_PASTA) {
                    File currentFile = new File(ARQUIVO_CONTEXTO.getPath(), s);
                    excluirSeExistir(currentFile);
                }
                ARQUIVO_CONTEXTO.delete();
            }
        }
    }

    public void criarSeNaoExistir(File ARQUIVO_CONTEXTO) {
        if (!ARQUIVO_CONTEXTO.exists()) {
            ARQUIVO_CONTEXTO.mkdirs();
        }
    }
}
